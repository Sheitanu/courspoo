<?php

/**
 * Description of Assure
 *
 * @author aheybeli
 */
class Assure {

    private $idAssure;
    private $nom;
    private $age;
    private $domicile;
    private $bonusMalus = 0;
    private static $information = "Tous les avantages ... !!";
    private $fidelite;

    //*************** Déclaration des constantes
    const BRONZE = 50; // La carte BRONZE s'acquière à partir de 50 points
    const ARGENT = 100;
    const OR = 150;

    function __construct($nom, $age, $domicile) {
        $this->setNom($nom);
        $this->setAge($age);
        $this->setDomicile($domicile);
        $this->setBonusMalus(0);
        $this->setFidelite(5);
    }

    public function reglerAssurance() {
        $this->setBonusMalus(4);
        $this->setFidelite(+10);
    }

    public function avoirAccident() {

        $this->setBonusMalus(-14);
    }

    public function parrainer(Assure $parraine) {
        $this->setFidelite(5);
        $parraine->setFidelite(5);
    }

    public function getBonusMalus() {

        return $this->bonusMalus;
    }

    function getNom() {

        return $this->nom;
    }

    function getAge() {
        return $this->age;
    }

    function getDomicile() {
        return $this->domicile;
    }

    static function getInformation() {

        return self::$information;
    }

    function getFidelite() {

        return $this->fidelite;
    }

    function getIdAssure() {
        return $this->idAssure;
    }

    function setNom($nom) {

        if (!is_string($nom) || empty($nom) || ctype_space($nom)) {
            trigger_error("Vous devez saisir un nom valide !", E_USER_WARNING);
            return;
        }
        $this->nom = $nom;
    }

    function setAge($age) {

        if (!is_integer($age) || empty($age) || ctype_space($age)) {
            trigger_error("Vous devez saisir un age valide !", E_USER_WARNING);
            return;
        }
        $this->age = $age;
    }

    function setDomicile($domicile) {
        if (!is_string($domicile) || empty($domicile) || ctype_space($domicile)) {
            trigger_error("Vous devez saisir une adresse valide", E_USER_WARNING);
            return;
        }
        $this->domicile = $domicile;
    }

    function setBonusMalus($bonusMalus) {
        if (!is_numeric($bonusMalus)) {
            trigger_error("Bonus / Malus sont des valeurs entières", E_USER_WARNING);
            return;
        }
        if (($this->getBonusMalus() + $bonusMalus) <= -50) {
            $this->bonusMalus = -50;
        } elseif (($this->getBonusMalus() + $bonusMalus) >= 50) {
            $this->bonusMalus = 50;
        } else {
            $this->bonusMalus = $this->getBonusMalus() + $bonusMalus;
        }
    }

    function setFidelite($fidelite) {

        if (!is_numeric($fidelite)) {
            trigger_error("Vous devez rentrer un nombre valide", E_USER_WARNING);
            return;
        }
        $this->fidelite = $this->getFidelite() + $fidelite;
    }

}
