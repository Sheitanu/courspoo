<!DOCTYPE html>


<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // Récupération d'un tableau d'objets (Cf. getListAssure())

        require_once 'Assure.php';
        require_once 'assureManager.php';
        ?>
        <h1>Les dossiers assurés</h1>
        <table class="table table-striped table-dark">
            <thead>
                <tr>
                    <th scope="col">N°</th>
                    <th scope="col">Nom</th>
                    <th scope="col">BonusMalus  </th>
                    <th scope="col">Fidelité</th>
                    <th scope="col">Régler</th>
                    <th scope="col">Notification</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                    <td><form><input type='button' value='Régler'></form></td>
                    <td><form><input type='button' value='Accident'></form></td>
                    <td><form><input type='button' value='Supprimer'></form></td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                    <td><form><input type='button' value='Régler'></form></td>
                    <td><form><input type='button' value='Accident'></form></td>
                    <td><form><input type='button' value='Supprimer'></form></td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>@twitter</td>
                    <td><form><input type='button' value='Régler'></form></td>
                    <td><form><input type='button' value='Accident'></form></td>
                    <td><form><input type='button' value='Supprimer'></form></td>
                </tr>
                <tr>
                    <th scope="row">4   </th>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>@twitter</td>
                    <td><form><input type='button' value='Régler'></form></td>
                    <td><form><input type='button' value='Accident'></form></td>
                    <td><form><input type='button' value='Supprimer'></form></td>
                </tr>
            </tbody>
        </table>
        <form action="sAssure1.php" method="POST">
            Nom<br>
            <input type="text" name="F_nom" required placeholder="Saisir le nom"value=""><br>
            Age<br>
            <input type="text" name="F_age" required placeholder="Saisir l'âge"value=""><br>
            Domicile<br>
            <input type="text" name="F_domicile" required placeholder="Saisir le domicile"value=""><br><br>
            <input type="submit" name='Créer' value='Créer'>
        </form>



    </body>
</html>
