<?php

class assureManager {

    private $cnx;

    public function __construct($cnx) {

        $this->setCnx($cnx);
    }

    public function count() {

    }

    function setCnx($cnx) {
        $this->cnx = $cnx;
    }

    public function addAssure(Assure $assure) {
        // Requête de type INSERT
        $sql = 'INSERT'
                . ' INTO assure (nom, age, domicile, bonusMalus, pointsFidelite)'
                . ' VALUES (?,?,?,?,?)';
    }

    public function editAssure(Assure $assure) {
        // Requête de type UPDATE
        $sql = 'UPDATE assure SET nom = ?, age = ?,'
                . 'domicile = ?, bonusMalus = ?, fidelite = ? WHERE idAssure = ?';
        $idRequete = $this->cnx->prepare($sql);
        $idRequete->execute(array($assure->getNom(),
            $assure->getAge(),
            $assure->getDomicile(),
            $assure->getBonusMalus(),
            $assure->getFidelite(),
            $assure->getIdAssure()));
    }

    public function deleteAssure(Assure $assure) {
        // Requête de type DELETE
        $sql = 'DELETE FROM assure WHERE idAssure = ?';
        $idRequete = $this->cnx->prepare($sql);
        $idRequete->execute(array($assure->getIdAssure));
    }

    public function getListAssure() {
        // Requête de type SELECT *
        $sql = 'SELECT * FROM assure';
        $idRequete = $this->cnx->query($sql);

        while ($row = $îdRequete->fetch(PDO::FETCH_ASSOC)) {
            $assure[] = new Assure($row);
        }

        return $assure;
    }

    public function getAssure($id) {
        // Requête de type SELECT 1 assure
        $row = array(); //Récupération des données de l'assuré

        $id = (int) $id;
        $sql = 'SELECT * FROM assure WHERE idAssure = ?';
        $idRequete = $this->$cnx->prepare($sql); //Requête préparée
        $idRequete->execute(array($id));
        $row = $idRequete->fetch(PDO::FETCH_ASSOC);

        return new Assure($row);
    }

}
