<?php

/**
 * Description of Assure
 *
 * @author aheybeli
 */
class Assure {

    private $nom;
    private $age;
    private $domicile;
    private $bonusMalus = 0;

    public function reglerAssurance() {
        $this->setBonusMalus(4);
    }

    public function avoirAccident() {

        $this->setBonusMalus(-14);
    }

    public function parrainer(Assure $parraine) {

        if ($this->getBonusMalus() > 0) {

            $parraine->setBonusMalus($this->getBonusMalus());
        } else {
            $parraine->setBonusMalus(4);
        }
    }

    public function getBonusMalus() {

        return $this->bonusMalus;
    }

    function getNom() {

        return $this->nom;
    }

    function getAge() {
        return $this->age;
    }

    function getDomicile() {
        return $this->domicile;
    }

    function setNom($nom) {

        if (!is_string($nom) || empty($nom) || ctype_space($nom)) {
            trigger_error("Vous devez saisir un nom valide !", E_USER_WARNING);
            return;
        }
        $this->nom = $nom;
    }

    function setAge($age) {

        if (!is_integer($age) || empty($age) || ctype_space($age)) {
            trigger_error("Vous devez saisir un age valide !", E_USER_WARNING);
            return;
        }
        $this->age = $age;
    }

    function setDomicile($domicile) {
        if (!is_string($domicile) || sempty($domicile) || ctype_space($domicile)) {
            trigger_error("Vous devez saisir une adresse valide", E_USER_WARNING);
            return;
        }
        $this->domicile = $domicile;
    }

    private function setBonusMalus($bonusMalus) {
        if (!is_numeric($bonusMalus)) {
            trigger_error("Bonus / Malus sont des valeurs entières", E_USER_WARNING);
            return;
        }
        if (($this->getBonusMalus() + $bonusMalus) <= -50) {
            $this->bonusMalus = -50;
        } elseif (($this->getBonusMalus() + $bonusMalus) >= 50) {
            $this->bonusMalus = 50;
        } else {
            $this->bonusMalus = $this->getBonusMalus() + $bonusMalus;
        }
    }

}
