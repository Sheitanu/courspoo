<!DOCTYPE html>


<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // *************** Définition des constantes de connexion *************** \\
        define('SERVEUR', 'localhost');
        define('UTILISATEUR', 'root');
        define('MOTDEPASSE', '');
        define('BD', 'bibliotheque');

        // *************** Connexion à la base de données *************** \\

        $cnx = new PDO('mysql:host=' . SERVEUR . '; dbname=' . BD, UTILISATEUR, MOTDEPASSE,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        // *************** Création d'u requête SQL *************** \\

        $sql = 'SELECT nom, prenom, date_naissance FROM auteur';

        // *************** Exécuter la requête SQL *************** \\

        $idRequete = $cnx->query($sql);

        while ($row = $idRequete->fetch(PDO::FETCH_ASSOC)) {
            echo $row['prenom'] . ' ' . $row['nom'] . ' ' . $row['date_naissance'] . '<br>'; // Syntaxe tableau, c'est plus du POO
        }

        $cnx = null;
        ?>
    </body>
</html>
