<?php

class pourHydratation {

    public function hydrater(array $data) {

        foreach ($data as $k => $v) {
            // Concaténation : nom de la méthode setter à appeler
            $setter = 'set' . ucfirst($k);

            // Appe si et seulement si la méthode existe !
            // Utiliser la fonction prédéfinies "method_exists."
            // Celle-ci attend 2 paramêtres : l'objet en cours et le nom de la méthode
            if (method_exists($this, $setter)) {
                //invoquer la méthode
                $this->$setter($v);
            }
        }
    }

    public function __construct($data) {
        $this->hydrater($data);
    }

}
