<?php

/**
 * Description of Assure
 *
 * @author aheybeli
 */
class Assure {

    private $nom;
    private $age;
    private $domicile;
    private $bonusMalus = 0;

    public function reglerAssurance() {
        $this->bonusMalus = $this->bonusMalus + 4;
    }

    public function avoirAccident() {

        $this->bonusMalus = $this->bonusMalus - 14;
    }

    public function parrainer(Assure $parraine) {

        if ($this->getBonusMalus() > 0) {

            $parraine->bonusMalus = $this->getBonusMalus();
        } else {
            $parraine->bonusMalus = 4;
        }
    }

    public function getBonusMalus() {

        return $this->bonusMalus;
    }

    function getNom() {

        return $this->nom;
    }

    function getAge() {
        return $this->age;
    }

    function getDomicile() {
        return $this->domicile;
    }

    function setNom($nom) {

        if (!is_string($nom) || empty($nom) || ctype_space($nom)) {
            trigger_error("Vous devez saisir un nom valide !", E_USER_WARNING);
            return;
        }
        $this->nom = $nom;
    }

    function setAge($age) {

        if (!is_integer($age) || empty($age) || ctype_space($age)) {
            trigger_error("Vous devez saisir un age valide !", E_USER_WARNING);
            return;
        }
        $this->age = $age;
    }

    function setDomicile($domicile) {
        if (!is_integer($domicile) || empty($domicile) || ctype_space($domicile)) {
            trigger_error("Vous devez saisir une adresse valide !", E_USER_WARNING);
            return;
        }
        $this->domicile = $domicile;
    }

    function setBonusMalus($bonusMalus) {
        $this->bonusMalus = $bonusMalus;
    }

}
