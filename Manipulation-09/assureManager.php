<?php

class assureManager {

    private $cnx;

    public function __construct($cnx) {

        $this->setCnx($cnx);
    }

    public function setCnx(PDO $cnx) {

        $this->cnx = $cnx;
    }

    public function addAssure(Assure $assure) {
        // Requete attendue type INSERT
        $sql = 'INSERT INTO assure (nom, age, domicile, bonusMalus, pointsFidelite)'
                . ' VALUES( ?, ?, ?, ?, ?)';
        $result = $this->cnx->prepare($sql); // Requête préparée
        $result->execute(array($assure->getNom(),
            $assure->getAge(),
            $assure->getDomicile(),
            $assure->getBonusMalus(),
            $assure->getPointsFidelite()
        ));
    }

    public function editAssure(Assure $assure) {
        // Requete attendue type UPDATE
        $sql = "UPDATE assure SET nom = ?, age = ?, domicile = ?, bonusMalus = ?,"
                . " pointsFidelite = ? WHERE idAssure = ?";
        $result = $this->cnx->prepare($sql); // Requête préparée
        $result->execute(array($assure->getNom(),
            $assure->getAge(),
            $assure->getDomicile(),
            $assure->getBonusMalus(),
            $assure->getPointsFidelite(),
            $assure->getIdAssure()
        ));
    }

    public function deleteAssure(Assure $assure) {
        // Requete attendue type DELETE
        $sql = 'DELETE FROM assure WHERE idAssure = ?';
        $result = $this->cnx->prepare($sql); // Requête préparée
        $result->execute(array($assure->getIdAssure()));
    }

    public function getListAssure() {
        // Requete attendue type SELECT (liste des assurés)
        $sql = 'SELECT * FROM assure';
        $result = $this->cnx->query($sql); // Requête simple

        while ($data = $result->fetch(PDO::FETCH_ASSOC)) {
            $assure[] = new Assure($data);
        }

        return $assure;
    }

    public function getAssure($id) {
        // Requete attendue type SELECT (1 assuré)
        // Elle pourrait prendre le nom de getId() !

        $id = (int) $id;
        $sql = 'SELECT * FROM assure WHERE idAssure = ?';
        $result = $this->cnx->prepare($sql); // Requête préparée
        $result->execute(array($id));
        $data = $result->fetch(PDO::FETCH_ASSOC);

        return new Assure($data);
    }

    public function count() {

        $sql = 'SELECT * FROM assure';
        $result = $this->cnx->query($sql); // Requête simple
        return $result->rowCount();

        //Autre manière d'écrire
//		$sql = 'SELECT COUNT(*) FROM assure';
//		$result = $this->cnx->query($sql); // Requête simple
//		return $result->fetchColumn();
        // Ou encore comme cela  :
//		return $this->cnx->query('SELECT COUNT(*) FROM assure')->fetchColumn();
    }

}
