
<?php

class Assure {

    private $idAssure;
    private $age;
    private $nom;
    private $domicile;
    private $bonusMalus;
    private $pointsFidelite;
    private $setter;
    private $message;
    private static $information = " Les avantages pour tout parrainage ... aaa ";

    const BRONZE = 50; // La carte bronze s'acquière à partir de 50 pts
    const ARGENT = 100; // La carte argent s'acquière à partir de 100 pts
    const OR = 150; // La carte or s'acquière à partir de 150 pts

    public function hydrater(array $row) {

        foreach ($row as $k => $v) {
            // Concaténation : nom de la méthode setter à appeler
            $setter = 'set' . ucfirst($k);
            // fonction prend 2 paramètres : l'objet en cours et le nom de la méthode
            if (method_exists($this, $setter)) {
                // Invoquer la méthode
                $this->$setter($v);
            }
        }
    }

    public function __construct($data) {

        // echo "Je suis le constructeur. Tout est en ordre !";
        $this->hydrater($data);
    }

    public function reglerAssurance() {

        $this->setBonusMalus(4);
        $this->setPointsFidelite(10);
    }

    public function avoirAccident() {

        $this->setBonusMalus(-14);
    }

    public function parrainer(Assure $parraine) {

        $parraine->setPointsFidelite(5);
        $this->setPointsFidelite(5);
    }

    /* --------- SETTER ------------- */

    public function setNom($nom) {

        if (!is_string($nom) || ctype_space($nom) || empty($nom) || strlen($nom) > 80) {

            $this->setMessage("Vous devez saisir un nom.");
            return false;
        }

        $this->nom = $nom;
    }

    public function setAge($age) {

        if (!is_numeric($age) || ( $age <= 0 || $age >= 130)) {

            $this->setMessage("Saisir l'âge (valeur entière entre 1 et 130 ans).");
            return false;
        }

        $this->age = $age;
    }

    public function setDomicile($domicile) {

        if (!is_string($domicile) || ctype_space($domicile) || empty($domicile) || strlen($domicile) > 80) {

            $this->setMessage("Vous devez saisir un lieu.");
            return false;
        }

        $this->domicile = $domicile;
    }

    public function setBonusMalus($bonusMalus) {


        if (!is_numeric($bonusMalus)) {

            // trigger_error("Bonus / malus sont des valeurs entières.", E_USER_WARNING);
            $this->setMessage("Bonus / malus sont des valeurs entières.");
            return false;
        }

        if (($this->getBonusMalus() + $bonusMalus) <= -50) {

            $this->bonusMalus = -50;
        } elseif (($this->getBonusMalus() + $bonusMalus) >= 50) {

            $this->bonusMalus = 50;
        } else {

            $this->bonusMalus = $this->getBonusMalus() + $bonusMalus;
        }
    }

    public function setPointsFidelite($pointsFidelite) {

        if (!is_numeric($pointsFidelite)) {

            $this->setMessage("Les points de fidélité sont des valeurs entières.");
            return false;
        }

        $this->pointsFidelite = $this->getPointsFidelite() + $pointsFidelite;
    }

    public function setMessage($message) {

        //Concaténation des messages d'erreur
        $this->message = $this->getMessage() . " " . $message;
    }

    private function setIdAssure($idAssure) {

        $this->idAssure = $idAssure;
    }

    /* --------- GETTER ------------- */

    public function getBonusMalus() {
        return $this->bonusMalus;
    }

    public function getAge() {
        return $this->age;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getDomicile() {
        return $this->domicile;
    }

    public function getPointsFidelite() {
        return $this->pointsFidelite;
    }

    public function getIdAssure() {
        return $this->idAssure;
    }

    public function getMessage() {
        return $this->message;
    }

    public static function getInformation() {

        return self::$information;
    }

}
