
<?php
require_once 'assure.php';
require_once 'assureManager.php';


// *************** Définition des constantes de connexion *************** \\
define('SERVEUR', 'localhost');
define('UTILISATEUR', 'root');
define('MOTDEPASSE', '');
define('BD', 'intervenants_zoo');

// *************** Connexion à la base de données *************** \\

$cnx = new PDO('mysql:host=' . SERVEUR . '; dbname=' . BD, UTILISATEUR, MOTDEPASSE,
        array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

$m = new intervenantManager($cnx);


if (isset($_POST['btn_creer'])) {

    $intervenant = new Intervenant(['nom' => $_POST['f_nom'],
        'prenom' => $_POST['f_prenom'],
        'statut' => $_POST['sel_statut']]);

    if ($intervenant->getNom() != null && $intervenant->getPrenom() != null && $intervenant->getStatut() != null) {
        $m->addIntervenant($intervenant);
    }
}

if (isset($_POST['btn_supprimer'])) {

    $intervenant = $m->getIntervenant($_POST['f_id']);

    var_dump($intervenant);
    $m->deleteIntervenant($intervenant);
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Les Intervenants</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="template/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>


        <br>
        <div class="container">

            <header class="row">

                <div class="col-md-12 hidden-sm hidden-xs"><img src="banner.webp"></div>

            </header>

            <div class="row pos">
                <nav class="col-md-3">

                    <ul class="list-group">
                        <li class="list-group-item"><a href="sAssure.php">Accueil</a>
                    </ul>
                </nav>

                <main class="col-md-9">
                    <h1>Les Intervenants</h1>
                    <!-- <p> Nombre d'intervenant : <strong>
                    </strong></p> -->
                    <table class="table">
                        <thead class="">
                            <tr>
                                <th scope="col">N°</th>
                                <th scope="col">Nom</th>
                                <th scope="col">Prénom</th>
                                <th scope="col">Statut</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
// Récupération d'un tableau d'objets (Cf. getListAssure())
                            $intervenants = $m->getListIntervenant();
//var_dump($intervenants);
                            if (empty($intervenants)) {
                                echo 'Aucun intervenants trouvé !! ';
                            } else {
                                foreach ($intervenants as $unIntervenant) {
                                    echo '<tr>
		<th scope="row">' . $unIntervenant->getIdEmployer() . '</th>
		<td>' . $unIntervenant->getNom() . '</td>
		<td>' . $unIntervenant->getPrenom() . '</td>
		<td>' . $unIntervenant->getStatut() . '</td>
		<td><form action="sAssure.php" method="POST">
		<input type="hidden" name="f_id" value="' . $unIntervenant->getIdEmployer() . '">
		<button type="submit" class="btn btn-primary btn-sm" name="btn_supprimer">Supprimer</button>
		</form>
		</td>
		</tr>';
                                }
                            }
                            ?>

                        </tbody>
                    </table>



                    <div class="row">

                        <div class="col-md-7">
                            <p>
                                <?php
                                if (isset($intervenant)) {
                                    echo "<em>" . $intervenant->getMessage() . "</em>";
                                }
                                ?>
                            </p>

                            <form method='POST' action='sAssure.php'>
                                <div class="form-group">
                                    <label for="f_nom">Nom</label>
                                    <input type="text" class="form-control" id="f_nom" name='f_nom' placeholder="Saisir le nom">
                                </div>
                                <div class="form-group">
                                    <label for="f_prenom">Prénom</label>
                                    <input type="text" class="form-control" id="f_prenom" name='f_prenom' placeholder="Saisir le prénom">
                                </div>
                                <div class="form-group">
                                    Saisir le statut
                                    <select name="sel_statut">
                                        <option value=""> Choisir</option>
                                        <option value="1. Utilisateur">Utilisateur</option>
                                        <option value="2. Modérateur">Modérateur</option>
                                        <option value="3. Administrateur"> Administrateur</option>
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary btn-sm" name='btn_creer'>Créer</button>



                            </form>
                        </div>
                        <div class="col-md-5"></div>

                    </div>
                </main>
            </div>

            <footer class="row pos">
                <div class="col-md-12 text-center">&copy; 2019 Campus-Centre - TP Manipulation 09 </div>
            </footer>

        </div>

    </body>
</html>