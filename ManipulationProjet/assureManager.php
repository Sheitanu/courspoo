<?php

class intervenantManager {

    private $cnx;

    public function __construct($cnx) {

        $this->setCnx($cnx);
    }

    public function setCnx(PDO $cnx) {

        $this->cnx = $cnx;
    }

    public function addIntervenant(Intervenant $intervenant) {
// Requete attendue type INSERT
        $sql = 'INSERT INTO intervenants (nom, prenom, statut)'
                . ' VALUES( ?, ?, ?)';
        $result = $this->cnx->prepare($sql); // Requête préparée
        $result->execute(array($intervenant->getNom(),
            $intervenant->getPrenom(),
            $intervenant->getStatut()
        ));
    }

    public function editIntervenant(Intervenant $intervenant) {
// Requete attendue type UPDATE
        $sql = "UPDATE intervenants SET nom = ?, prenom = ?, statut = ? WHERE idEmployer = ?";
        $result = $this->cnx->prepare($sql); // Requête préparée
        $result->execute(array($intervenant->getNom(),
            $intervenant->getPrenom(),
            $intervenant->getStatut()
        ));
    }

    public function deleteIntervenant(Intervenant $intervenant) {
// Requete attendue type DELETE
        $sql = 'DELETE FROM intervenants WHERE idEmployer = ?';
        $result = $this->cnx->prepare($sql); // Requête préparée
        $result->execute(array($intervenant->getIdEmployer()));
    }

    public function getListIntervenant() {
// Requete attendue type SELECT (liste des assurés)
        $sql = 'SELECT * FROM intervenants';
        $result = $this->cnx->query($sql); // Requête simple
        //var_dump($result);
        while ($data = $result->fetch(PDO::FETCH_ASSOC)) {
            $intervenant[] = new Intervenant($data);
        }

        return $intervenant;
    }

    public function getIntervenant($id) {
// Requete attendue type SELECT (1 assuré)
// Elle pourrait prendre le nom de getId() !
        $id = (int) $id;
        $sql = 'SELECT * FROM intervenants WHERE idEmployer = ?';
        $result = $this->cnx->prepare($sql); // Requête préparée
        $result->execute(array($id));
        $data = $result->fetch(PDO::FETCH_ASSOC);

        return new Intervenant($data);
    }

//Autre manière d'écrire
//		$sql = 'SELECT COUNT(*) FROM intervenants';
//		$result = $this->cnx->query($sql); // Requête simple
//		return $result->fetchColumn();
// Ou encore comme cela  :
//		return $this->cnx->query('SELECT COUNT(*) FROM assure')->fetchColumn();
}
