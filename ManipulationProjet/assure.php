
<?php

class Intervenant {

    private $idEmployer;
    private $nom;
    private $prenom;
    private $statut;
    private $setter;
    private $message;
    private static $information = " Les avantages pour tout parrainage ... aaa ";

    const BRONZE = 50; // La carte bronze s'acquière à partir de 50 pts
    const ARGENT = 100; // La carte argent s'acquière à partir de 100 pts
    const OR = 150; // La carte or s'acquière à partir de 150 pts

    public function hydrater(array $row) {

        foreach ($row as $k => $v) {
            // Concaténation : nom de la méthode setter à appeler
            $setter = 'set' . ucfirst($k);
            // fonction prend 2 paramètres : l'objet en cours et le nom de la méthode
            if (method_exists($this, $setter)) {
                // Invoquer la méthode
                $this->$setter($v);
            }
        }
    }

    public function __construct($data) {

        // echo "Je suis le constructeur. Tout est en ordre !";
        $this->hydrater($data);
    }

    /* --------- SETTER ------------- */

    public function setNom($nom) {

        if (!is_string($nom) || ctype_space($nom) || empty($nom) || strlen($nom) > 80) {

            $this->setMessage("Vous devez saisir un nom.");
            return false;
        }

        $this->nom = $nom;
    }

    function setPrenom($prenom) {

        if (!is_string($prenom) || ctype_space($prenom) || empty($prenom) || strlen($prenom) > 80) {

            $this->setMessage("Vous devez saisir un prénom.");
            return false;
        }

        $this->prenom = $prenom;
    }

    public function setMessage($message) {

        //Concaténation des messages d'erreur
        $this->message = $this->getMessage() . " " . $message;
    }

    public function setIdEmployer($idEmployer) {
        $this->idEmployer = $idEmployer;
    }

    public function setStatut($statut) {

        $this->statut = $statut;
    }

    /* --------- GETTER ------------- */

    public function getNom() {

        return $this->nom;
    }

    function getPrenom() {
        return $this->prenom;
    }

    public function getIdEmployer() {
        return $this->idEmployer;
    }

    public function getStatut() {

        return $this->statut;
    }

    public function getMessage() {
        return $this->message;
    }

    public static function getInformation() {

        return self::$information;
    }

}
