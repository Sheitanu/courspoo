<?php

require_once 'Assure.php';
//Création de 2 assurés
$obS = new Assure('Serge', 40, 'Espagne');
$obF = new Assure('Fadhel', 45, 'Tunisie');
//Le premier règle son assurance
$obS->reglerAssurance();
// puis parraine le second !
$obS->parrainer($obF); 
// Le second est victime d'un accident sans gravité ...
$obF->avoirAccident();
// et règle son assurance en suivant.
$obF->reglerAssurance();
//Le premier négocie : 20 points de bonus + 20 points sur sa carte ...
$obS->setBonusMalus(20);
$obS->setPointsFidelite(20);
$obS->reglerAssurance();
		
printf("L'assuré Serge :<br>");
printf("Les points de fidélité de Serge sont au nombre de : %d  <br>", $obS->getPointsFidelite());
printf("Le pourcentage de bonus malus de Serge est : %d %% <br>", $obS->getBonusMalus());

printf("<br><br>");
printf("L'assuré Fadhel :<br>");
printf("Les points de fidélité de Fadhel sont au nombre de : %d  <br>", $obF->getPointsFidelite());
printf("Le pourcentage de bonus malus de Fadhel est : %d %% <br>", $obF->getBonusMalus());









