<?php

/**
 * Description of Assure
 *
 * @author tvosgiens
 */
class Assure {

	// =======  Déclaration des propriétés / attributs
	private $nom;
	private $age;
	private $domicile;
	private $bonusMalus;
	private $pointsFidelite;
	private static $information = "Tous les avantages ... !!";

	//======= Déclaration des constantes
	const BRONZE = 50; // La carte BRONZE s'acquière à partir de 50 points
	const ARGENT = 100;
	const OR = 150;

	function __construct($nom, $age, $domicile) {

		$this->setNom($nom);
		$this->setAge($age);
		$this->setDomicile($domicile);
		$this->setBonusMalus(0);
		$this->setPointsFidelite(5);
	}

	// =======  Définition des méthodes 
	public function reglerAssurance() {

		//$this->bonusMalus = $this->bonusMalus + 4;
		$this->setBonusMalus(4);
		$this->setPointsFidelite(10);
	}

	public function avoirAccident() {

		//$this->bonusMalus = $this->bonusMalus - 14;
		$this->setBonusMalus(-14);
	}

	public function parrainer(Assure $a) {

		$this->setPointsFidelite(5);
		$a->setPointsFidelite(5);
		
	}

	/*
	 * Getters --------------
	 */

	public function getNom() {

		return $this->nom;
	}

	public function getAge() {

		return $this->age;
	}

	public function getDomicile() {

		return $this->domicile;
	}

	public function getBonusMalus() {

		return $this->bonusMalus;
	}

	function getPointsFidelite() {

		return $this->pointsFidelite;
	}

	static function getInformation() {

		return self::$information;
	}

	/*
	 * Setters --------------
	 */

	public function setNom($nom) {

		if (!is_string($nom) || ctype_space($nom) || empty($nom)) {

			trigger_error("Vous devez saisir un nom valide.", E_USER_WARNING);
			return;
		}

		$this->nom = $nom;
	}

	public function setAge($age) {

		if (!is_numeric($age) || ( $age <= 0 || $age >= 130)) {

			trigger_error("Saisir l'âge (valeur entière entre 1 et 130 ans).", E_USER_WARNING);
			return;
		}

		$this->age = $age;
	}

	public function setDomicile($domicile) {

		if (!is_string($domicile) || ctype_space($domicile) || empty($domicile)) {

			trigger_error("Vous devez saisir un lieu valide.", E_USER_WARNING);
			return;
		}

		$this->domicile = $domicile;
	}

	// Cette méthode pourrait être privée !
	public function setBonusMalus($bonusMalus) {


		if (!is_numeric($bonusMalus)) {

			trigger_error("Bonus / malus sont des valeurs entières.", E_USER_WARNING);
			return;
		}

		if (($this->getBonusMalus() + $bonusMalus) <= -50) {

			$this->bonusMalus = -50;
		} elseif (($this->getBonusMalus() + $bonusMalus) >= 50) {

			$this->bonusMalus = 50;
		} else {

			$this->bonusMalus = $this->getBonusMalus() + $bonusMalus;
		}
	}

	function setPointsFidelite($pointsFidelite) {

		if (!is_numeric($pointsFidelite)) {
			trigger_error("Les points de fidélité sont des valeurs entières.", E_USER_WARNING);
			return;
		}

		// Cumul des points de fidélité
		$this->pointsFidelite = $this->getPointsFidelite() + $pointsFidelite;
	}

}
